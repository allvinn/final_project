package edu.gvsu.cis.allvinn.Final_Project;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: allvinn
 * Date: 4/10/14
 * Time: 11:58 AM
 * To change this template use File | Settings | File Templates.
 */



public class StateList extends ListActivity {

    public ArrayList<StringBuffer> states;
    public ArrayAdapter<StringBuffer> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);    //To change body of overridden methods use File | Settings | File Templates.


    states = new ArrayList<StringBuffer>();

    states.add(new StringBuffer("Alabama"));
    states.add(new StringBuffer("Alaska"));
    states.add(new StringBuffer("Arizona"));
    states.add(new StringBuffer("Arkansas"));
    states.add(new StringBuffer("California"));
    states.add(new StringBuffer("Colorado"));
    states.add(new StringBuffer("Connecticut"));
    states.add(new StringBuffer("Delaware"));
    states.add(new StringBuffer("Florida"));
    states.add(new StringBuffer("Georgia"));
    states.add(new StringBuffer("Hawaii"));
    states.add(new StringBuffer("Idaho"));
    states.add(new StringBuffer("Illinois"));
    states.add(new StringBuffer("Indiana"));
    states.add(new StringBuffer("Iowa"));
    states.add(new StringBuffer("Kansas"));
    states.add(new StringBuffer("Kentucky"));
    states.add(new StringBuffer("Louisana"));
    states.add(new StringBuffer("Maine"));
    states.add(new StringBuffer("Maryland"));
    states.add(new StringBuffer("Massachusetts"));
    states.add(new StringBuffer("Michigan"));
    states.add(new StringBuffer("Minnesota"));
    states.add(new StringBuffer("Mississippi"));
    states.add(new StringBuffer("Missouri"));
    states.add(new StringBuffer("Montana"));
    states.add(new StringBuffer("Nebraska"));
    states.add(new StringBuffer("Nevada"));
    states.add(new StringBuffer("New Hampshire"));
    states.add(new StringBuffer("New Jersey"));
    states.add(new StringBuffer("New Mexico"));
    states.add(new StringBuffer("New York"));
    states.add(new StringBuffer("North Carolina"));
    states.add(new StringBuffer("North Dakota"));
    states.add(new StringBuffer("Ohio"));
    states.add(new StringBuffer("Oklahoma"));
    states.add(new StringBuffer("Oregon"));
    states.add(new StringBuffer("Pennsylvania"));
    states.add(new StringBuffer("Rhode Island"));
    states.add(new StringBuffer("South Carolina"));
    states.add(new StringBuffer("South Dakota"));
    states.add(new StringBuffer("Tennessee"));
    states.add(new StringBuffer("Texas"));
    states.add(new StringBuffer("Utah"));
    states.add(new StringBuffer("Vermont"));
    states.add(new StringBuffer("Virginia"));
    states.add(new StringBuffer("Washington"));
    states.add(new StringBuffer("West Virginia"));
    states.add(new StringBuffer("Wisconsin"));
    states.add(new StringBuffer("Wyoming"));

    adapter = new ArrayAdapter<StringBuffer> (this, R.layout.statelist,R.id.stateView, states);

    setListAdapter(adapter);
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);    //To change body of overridden methods use File | Settings | File Templates.
        Log.d("Nathan Allvin", "Item selected at position " + position);
        /*retrieve the selected item*/

            StringBuffer selection =  states.get(position);
            Intent detailIntent = new Intent(StateList.this, Artists.class);
            detailIntent.putExtra("selectedState", selection.toString());
            startActivity(detailIntent);

    }
}
