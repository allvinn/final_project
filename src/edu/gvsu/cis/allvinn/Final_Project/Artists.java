package edu.gvsu.cis.allvinn.Final_Project;

/**
 * Created with IntelliJ IDEA.
 * User: allvinn
 * Date: 4/9/14
 * Time: 8:46 PM
 * To change this template use File | Settings | File Templates.
 */

import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.*;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Artists extends ListActivity implements SimpleAdapter.ViewBinder {
    private ArrayList<Map<String,Object>> Artists;
    private int currentId = 0, flag =0;
    private String country= "", city = "", state = "", locationUrl = "",twitter = "&bucket=id:twitter";
    private	SimpleAdapter	ArtistAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent info = getIntent(); /* retrieve the incoming Intent */
        if (info != null) {
            if(!(info.getStringExtra("selectedCountry")==null))
            country = info.getStringExtra("selectedCountry");
            if(!(info.getStringExtra("selectedState")==null))
            state = info.getStringExtra("selectedState");
            if(!(info.getStringExtra("selectedCity")==null))
            city = info.getStringExtra("selectedCity");
        }

        locationUrl = UrlSetup (country, state, city);
View header = getLayoutInflater().inflate(
        R.layout.header, null, false);
        getListView().addHeaderView(header);
        TextView textBox = (TextView)header.findViewById(R.id.location);
        String location = locationUrl;
        if(location.contains(":")){
        String delims ="[:]";
        String[] split = location.split(delims);
        location = split[1];
        }
        location = location.replaceAll("\\+", " ");

        textBox.setText(location);


        Artists = new ArrayList<Map<String,Object>>();
        ArtistAdapter	=	new	SimpleAdapter(this,
                Artists,
                R.layout.artist_item,
                new	String[]	{"name","genre","image","twitter"},
                new	int[]	{R.id.main_text,	R.id.sub_text,	R.id.img, R.id.twitter}
        );
        setListAdapter(ArtistAdapter);
        ArtistAdapter.setViewBinder(this);
        getListView().setBackgroundResource(R.drawable.music);
        new ArtistAsync().execute();
    }

    @Override
    public boolean setViewValue(View view, final Object o, String s) {
        if	(view.getId()	==	R.id.img)	{
            /*	we	are	interested	in	binding	only	the	image	*/
            ImageView pic	=	(ImageView)	view;
            /*	place	the	image	on	the	list	item	*/
            pic.setImageResource(R.drawable.ic_unknown_member);
            if(o != null)
            pic.setImageBitmap((Bitmap) o);

            return	true;	/*	no	further	processing	by	SimpleAdapter needed	*/

        }if (view.getId() == R.id.twitter) {
            Button b=(Button) view;
            b.setEnabled(false);
            b.setFocusable(false);
            b.setBackgroundResource(R.drawable.disabled);
            if(o !=  null){
             final String check = o.toString();
             if(!check.equals("")){
                b.setEnabled(true);
                b.setBackgroundResource(R.drawable.green_glossy);
                b.setOnClickListener(new View.OnClickListener(){

                    public void onClick(View v) {
                        try {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("twitter://user?screen_name=" + check)));
                        }catch (Exception e) {
                            startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/#!/" + check)));
                        }
                    }
                });
            }
            }
            return true;
        }else
         /*	tell	SimpleAdapter	to	use	default	binding	*/
            return false;
    }

    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        if(!Artists.get(position).get("name").equals("Can not find artists from the specified region.")){

        /* create an intent to start a new activity */
            Intent detailIntent = new Intent(this, Songs.class);
        /* hand over the selected item via the Intent */
        detailIntent.putExtra ("id", Artists.get(position).get("id").toString());
        detailIntent.putExtra ("name", Artists.get(position).get("name").toString());

        startActivity(detailIntent);
        }
    }

    /* background task for downloading JSON data from the web service */
    private class ArtistAsync extends AsyncTask<Void,Integer,Void> {

        @Override
        protected Void doInBackground(Void... weDontUseThisParameter) {
            URL ArtistUrl;
            try {
                if(locationUrl.contains("Grand+Rapids")){
                Map<String,Object> newArtist = new HashMap<String,Object>();
                newArtist.put("name", "Jake Vroon");
                newArtist.put("genre","Sick Country");
                newArtist.put("id", "ARSIVDJ1457556A176");
                newArtist.put("twitter","jakevroon");
                Artists.add(newArtist);
            }
                String url = "http://developer.echonest.com/api/v4/artist/search?api_key=YO7CGFUIDSW5KCCET&format=json&artist_location="+locationUrl+"&bucket=genre" + twitter;
                ArtistUrl = new URL(url);
                String out = "";
                Scanner scan = new Scanner(ArtistUrl.openStream());
                while (scan.hasNextLine())
                    out += scan.nextLine();
                JSONObject ArtistObj  = new JSONObject(out);
                JSONArray ArtistArray = ArtistObj.getJSONObject("response").getJSONArray("artists");
                Log.d("String","string2" + ArtistArray.length());

                for (int m = 0; m < ArtistArray.length(); m++)
                {
                    Map<String,Object> newArtist = new HashMap<String,Object>();
                    JSONObject AO = ArtistArray.getJSONObject(m);
                    newArtist.put("name", AO.getString("name"));
                    if(!(AO.getJSONArray("genres").isNull(0))){
                    newArtist.put("genre", AO.getJSONArray("genres").getJSONObject(0).getString("name"));
                    }else{
                        newArtist.put("genre","Unknown");
                    }
                    newArtist.put("id", AO.getString("id"));
                    if(AO.has("foreign_ids")){
                    newArtist.put("twitter",TwitterHandle(AO.getJSONArray("foreign_ids").getJSONObject(0).getString("foreign_id")));
                    }else
                        newArtist.put("twitter","");
                    Artists.add(newArtist);
                }



            } catch (MalformedURLException e) {
                e.printStackTrace();
                Log.d("fail","fail:"+ e.getMessage()) ;
            } catch (IOException e) {
                e.printStackTrace();
                Log.d("fail","fail:"+ e.getMessage()) ;
            } catch(JSONException e) {
                e.printStackTrace();
                Log.d("fail","fail:"+ e.getMessage());
            }
            return null;
        }
        public String TwitterHandle(String handle){
            String delims ="[:]";
            String[] split = handle.split(delims);
            return split[2];
        }
        @Override
        protected void onPreExecute() {
            /* activate the spinning indicator */
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected void onPostExecute(Void unusedParameter) {
            /* de-activate the spinning indicator */
            setProgressBarIndeterminateVisibility(false);

            /* Tell the adapter that we have a new data to render */
            ArtistAdapter.notifyDataSetChanged();
            if((Artists.size() > 0) && ((flag == 1) || (Artists.size() > 5))){
            new ArtistImages().execute();
            }else if(flag == 0){
                flag = 1;
                twitter = "";
                new ArtistAsync().execute();
            }else{
                Map<String,Object> newArtist = new HashMap<String,Object>();
                newArtist.put("name", "Can not find artists from the specified region.");
                Artists.add(newArtist);
            }

        }
    }
    private class ArtistImages extends AsyncTask<Void,Integer,Void> {

        @Override
        protected Void doInBackground(Void... weDontUseThisParameter) {
            URL ArtistUrl;
            try {
                if(Artists.get(currentId).get("id").toString() != "ARSIVDJ1457556A176"){
                String url = "http://developer.echonest.com/api/v4/artist/images?api_key=YO7CGFUIDSW5KCCET&id=" + Artists.get(currentId).get("id").toString() + "&format=json&results=1&start=0&license=unknown";
                ArtistUrl = new URL(url);
                String out = "";
                Scanner scan = new Scanner(ArtistUrl.openStream());
                while (scan.hasNextLine())
                    out += scan.nextLine();
                JSONObject ArtistObj  = new JSONObject(out);
                URL imgUrl = new URL(ArtistObj.getJSONObject("response").getJSONArray("images").getJSONObject(0).getString("url"));
                Bitmap bitmap;
                InputStream input = imgUrl.openStream();
                // Decode Bitmap
                bitmap = BitmapFactory.decodeStream(input);
                Artists.get(currentId).put("image", bitmap);
                }else{
                    Artists.get(currentId).put("image",BitmapFactory.decodeResource(getResources(),
                            R.drawable.ic_jakevroon));
                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return null;
        }
        @Override
        protected void onPreExecute() {
            /* activate the spinning indicator */
            setProgressBarIndeterminateVisibility(true);
        }

        @Override
        protected void onPostExecute(Void unusedParameter) {
            /* de-activate the spinning indicator */
            setProgressBarIndeterminateVisibility(false);

            /* Tell the adapter that we have a new data to render */
            ArtistAdapter.notifyDataSetChanged();
            if(currentId < Artists.size()-1){
                currentId +=1;
                new ArtistImages().execute();
            }

        }
    }

    public static String UrlSetup (String Country, String State, String City)
    {
        String url;
        if (!City.equals("")) {
            String delims ="[,]";
            String[] split = City.split(delims);
            City = ("city:"+split[0]);
            City = City.replace(' ','+');
        }
        if(!State.equals("")) {
            State = ("region:"+State);
            State = State.replace(' ','+');
        }
        if (!Country.equals("")) {
            Country = Country.replace(' ','+');
        }
        url = (Country+"+"+State+"+"+City);

        if (url.charAt(0) == '+') {
            if (url.charAt(1) == '+') {
                url = url.substring(2);
            }
            else {
                url = url.substring(1);
            }
        }
        return url;
    }
}
