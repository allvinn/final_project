package edu.gvsu.cis.allvinn.Final_Project;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class MyActivity extends Activity {

    private Button manualSearch, autoSearch;
    private TextView title;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        manualSearch =  (Button) findViewById(R.id.manualSearch);
        autoSearch = (Button) findViewById(R.id.autoSearch);

        title = (TextView) findViewById(R.id.title);

        manualSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent detailIntent;
                detailIntent = new Intent(MyActivity.this, ManualSearch.class);
                MyActivity.this.startActivity(detailIntent);
            }
        });

        autoSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent detailIntent;
                detailIntent = new Intent(MyActivity.this, LocationSearch.class);
                startActivity(detailIntent);
           }
       });
    }


}