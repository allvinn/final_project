package edu.gvsu.cis.allvinn.Final_Project;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.TreeMap;

public class LocationSearch extends Activity implements LocationListener {

    private LocationManager locationManager;
    private String provider;
    private ArrayList<String> list;
    private TreeMap<String, String> map;
    private Scanner s;
    private double myLat, myLon;


    /** Called when the activity is first created. */

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.blank);

        // Get the location manager
        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        // Define the criteria how to select the location provider -> use
        // default
        Criteria criteria = new Criteria();
        provider = locationManager.getBestProvider(criteria, false);
        Location location = locationManager.getLastKnownLocation(provider);

        list = new ArrayList<String>();
        map = new TreeMap<String, String>();

        try {
            PlayWithRawFiles();
        } catch (IOException e) {
            Toast.makeText(getApplicationContext(),
                    "Problems: " + e.getMessage(), 1).show();
        }

    }

    public void PlayWithRawFiles() throws IOException {
        String str="";

        InputStream is = getAssets().open("cities");
        BufferedReader reader = new BufferedReader(new InputStreamReader(is));
        if (is!=null) {
            while ((str = reader.readLine()) != null) {

                s = new Scanner(str).useDelimiter(" : ");
                String city = s.next();
                String coords = s.next();
                if (coords != null)
                {
                    list.add(coords);
                    map.put(coords, city);
                }
            }
        }
        is.close();


    }

    public String findCity()
    {
        double cityLat, cityLon;
        String cLat, cLon;
        String ila = "No nearby";
        String ilo = "city :(";
        double shortestDis = 9999;
        double newDistance = 0;
        for (String j : list)
        {
            s = new Scanner(j).useDelimiter(" ");
            cLat = s.next();
            cLon = s.next();
            cityLat = Double.parseDouble(cLat);
            cityLon = Double.parseDouble(cLon);
            newDistance = calculateDistance(myLat, cityLat, myLon, cityLon);

            if (newDistance < shortestDis)
            {
                shortestDis = newDistance;
                ila = cLat;
                ilo = cLon;
            }

        }
        return ila + " " + ilo;
    }

    public double calculateDistance(double x1, double x2, double y1, double y2)
    {
        double dis = 0;
        double xDis = x2 - x1;
        double yDis = y2 - y1;
        xDis = xDis * xDis;
        yDis = yDis * yDis;
        dis = Math.sqrt(xDis + yDis);
        return dis;
    }

    /* Request updates at startup */
    @Override
    protected void onResume() {
        super.onResume();
        locationManager.requestLocationUpdates(provider, 400, 1, this);
    }

    @Override
    protected void onPause() {
        super.onPause();
        locationManager.removeUpdates(this);
    }

    @Override
    public void onLocationChanged(Location location) {
        myLat = location.getLatitude();
        myLon = location.getLongitude();

        String cityName = map.get(findCity());

        Intent detailIntent = new Intent(LocationSearch.this, Artists.class);
        detailIntent.putExtra("selectedCity", cityName);
        startActivity(detailIntent);
        finish();
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
        // TODO Auto-generated method stub

    }

    @Override
    public void onProviderEnabled(String provider) {
        Toast.makeText(this, "Enabled new provider " + provider,
                Toast.LENGTH_SHORT).show();

    }

    @Override
    public void onProviderDisabled(String provider) {
        Toast.makeText(this, "Disabled provider " + provider,
                Toast.LENGTH_SHORT).show();
    }
}