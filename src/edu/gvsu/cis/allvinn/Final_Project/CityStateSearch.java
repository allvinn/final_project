package edu.gvsu.cis.allvinn.Final_Project;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: allvinn
 * Date: 4/10/14
 * Time: 11:11 AM
 * To change this template use File | Settings | File Templates.
 */
public class CityStateSearch extends MyActivity{

    private Button citiesplz, statesplz;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.cityorstate);

        citiesplz = (Button) findViewById(R.id.citiesPlz);
        statesplz = (Button) findViewById(R.id.statesPlz);

        citiesplz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent detailIntent;
                detailIntent = new Intent(CityStateSearch.this, CityList.class);
                CityStateSearch.this.startActivity(detailIntent);
            }
        });

        statesplz.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent detailIntent;
                detailIntent = new Intent(CityStateSearch.this, StateList.class);
                CityStateSearch.this.startActivity(detailIntent);
            }
        });
    }
}
