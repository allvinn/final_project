package edu.gvsu.cis.allvinn.Final_Project;

import android.app.ListActivity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import java.util.ArrayList;

/**
 * Created with IntelliJ IDEA.
 * User: allvinn
 * Date: 4/8/14
 * Time: 4:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class ManualSearch extends ListActivity {

    public ArrayList<StringBuffer> countries;
    public ArrayAdapter<StringBuffer> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        countries = new ArrayList<StringBuffer>();
        //setContentView(R.layout.main);
        countries.add(new StringBuffer("United States"));
        countries.add(new StringBuffer("United Kingdom"));
        countries.add(new StringBuffer("Afghanistan"));
        countries.add(new StringBuffer("Algeria"));
        countries.add(new StringBuffer("Argentina"));
        countries.add(new StringBuffer("Australia"));
        countries.add(new StringBuffer("Bangladesh"));
        countries.add(new StringBuffer("Brazil"));
        countries.add(new StringBuffer("Burma"));
        countries.add(new StringBuffer("Canada"));
        countries.add(new StringBuffer("China"));
        countries.add(new StringBuffer("Colombia"));
        countries.add(new StringBuffer("Democratic Republic of the Congo"));
        countries.add(new StringBuffer("Egypt"));
        countries.add(new StringBuffer("Ethiopia"));
        countries.add(new StringBuffer("France"));
        countries.add(new StringBuffer("Germany"));
        countries.add(new StringBuffer("Ghana"));
        countries.add(new StringBuffer("India"));
        countries.add(new StringBuffer("Indonesia"));
        countries.add(new StringBuffer("Iran"));
        countries.add(new StringBuffer("Iraq"));
        countries.add(new StringBuffer("Italy"));
        countries.add(new StringBuffer("Japan"));
        countries.add(new StringBuffer("Kenya"));
        countries.add(new StringBuffer("Malaysia"));
        countries.add(new StringBuffer("Mexico"));
        countries.add(new StringBuffer("Morocco"));
        countries.add(new StringBuffer("Mozambique"));
        countries.add(new StringBuffer("Nepal"));
        countries.add(new StringBuffer("Netherlands"));
        countries.add(new StringBuffer("New Zealand"));
        countries.add(new StringBuffer("Nigeria"));
        countries.add(new StringBuffer("North Korea"));
        countries.add(new StringBuffer("Norway"));
        countries.add(new StringBuffer("Pakistan"));
        countries.add(new StringBuffer("Peru"));
        countries.add(new StringBuffer("Philippines"));
        countries.add(new StringBuffer("Poland"));
        countries.add(new StringBuffer("Portugal"));
        countries.add(new StringBuffer("Puerto Rico"));
        countries.add(new StringBuffer("Russia"));
        countries.add(new StringBuffer("Saudi Arabia"));
        countries.add(new StringBuffer("South Africa"));
        countries.add(new StringBuffer("South Korea"));
        countries.add(new StringBuffer("Spain"));
        countries.add(new StringBuffer("Sudan"));
        countries.add(new StringBuffer("Sweden"));
        countries.add(new StringBuffer("Switzerland"));
        countries.add(new StringBuffer("Tanzania"));
        countries.add(new StringBuffer("Thailand"));
        countries.add(new StringBuffer("Turkey"));
        countries.add(new StringBuffer("Uganda"));
        countries.add(new StringBuffer("Ukraine"));
        countries.add(new StringBuffer("Uzbekistan"));
        countries.add(new StringBuffer("Venezuela"));
        countries.add(new StringBuffer("Vietnam"));
        countries.add(new StringBuffer("Yemen"));

        adapter = new ArrayAdapter<StringBuffer>(this,R.layout.countries,R.id.countriesList, countries);
        setListAdapter(adapter);
    }


    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);    //To change body of overridden methods use File | Settings | File Templates.
        Log.d("Nathan Allvin", "Item selected at position " + position);
        /*retrieve the selected item*/
        if (position == 0) {
            //StringBuffer selection = countries.get(position);
            Intent detailIntent = new Intent(ManualSearch.this, CityStateSearch.class);
            //detailIntent.putExtra("selectedCountry", selection.toString());
            startActivity(detailIntent);

        }
        else {
            StringBuffer selection = countries.get(position);
            Intent detailIntent = new Intent(ManualSearch.this, Artists.class);
            detailIntent.putExtra("selectedCountry", selection.toString());
            startActivity(detailIntent);

        }
    }
}
