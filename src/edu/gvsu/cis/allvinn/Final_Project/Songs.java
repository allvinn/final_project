package edu.gvsu.cis.allvinn.Final_Project;

/**
 * Created with IntelliJ IDEA.
 * User: allvinn
 * Date: 4/11/14
 * Time: 1:21 PM
 * To change this template use File | Settings | File Templates.
 */
import android.app.ListActivity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;

import android.widget.ImageView;
import android.widget.ListView;
import android.widget.SimpleAdapter;
import android.widget.TextView;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;


public class Songs extends ListActivity implements SimpleAdapter.ViewBinder{
    private ArrayList<Map<String,Object>> Songs;
    private String Id = "",name = "";
    private	SimpleAdapter SongAdapter;
    private TextView artist;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Intent info = getIntent(); /* retrieve the incoming Intent */
        if (info != null) {
            Id = info.getStringExtra("id");
            name = info.getStringExtra("name");
        }
        Songs = new ArrayList<Map<String,Object>>();
        SongAdapter =	new	SimpleAdapter(this,
                Songs,
                R.layout.songs_item,
                new	String[]	{"title","name","image"},
                new	int[]	{R.id.main_text, R.id.sub_text,	R.id.img}
        );

        setListAdapter(SongAdapter);
        SongAdapter.setViewBinder(this);
        getListView().setBackgroundResource(R.drawable.colorful);
        new SongsAsync().execute();

    }

    @Override
    public boolean setViewValue(View view, Object o, String s) {
        if	(view.getId()	==	R.id.img)	{
            /*	we	are	interested	in	binding	only	the	image	*/
            ImageView pic	=	(ImageView)	view;
            /*	place	the	image	on	the	list	item	*/
            pic.setImageResource(R.drawable.ic_xmms2_logo_white_128);

            return	true;	/*	no	further	processing	by	SimpleAdapter needed	*/
        }else
         /*	tell	SimpleAdapter	to	use	default	binding	*/
            return false;
    }
    @Override
    protected void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);

        String str = "https://market.android.com/search?q=%s&c=music&featured=MUSIC_STORE_SEARCH",
             Search = Songs.get(position).get("title").toString() + " " + Songs.get(position).get("name").toString();
        Intent shopIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(String.format(str,
                Uri.encode(Search))));
        shopIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        shopIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(shopIntent);
    }

    /* background task for downloading JSON data from the web service */
    private class SongsAsync extends AsyncTask<Void,Integer,Void> {

        @Override
        protected Void doInBackground(Void... weDontUseThisParameter) {
            URL SongsUrl;
            try {
                String url = "http://developer.echonest.com/api/v4/artist/songs?api_key=YO7CGFUIDSW5KCCET&format=json&id=" + Id + "&results=30";
                SongsUrl = new URL(url);
                String out = "";
                Scanner scan = new Scanner(SongsUrl.openStream());
                while (scan.hasNextLine())
                    out += scan.nextLine();
                JSONObject ArtistObj  = new JSONObject(out);
                JSONArray SongsArray = ArtistObj.getJSONObject("response").getJSONArray("songs");
                int skip = 0;
                for (int m = 0; m < SongsArray.length()-1; m++)
                {
                    skip = 0;
                    Map<String,Object> newSong = new HashMap<String,Object>();
                    JSONObject As = SongsArray.getJSONObject(m);
                    for(int c = 0; c < Songs.size(); c++){
                        if(Songs.get(c).get("title").toString().equalsIgnoreCase((As.getString("title")))){
                            skip = 1;
                        }
                    }
                    if(skip == 0){
                        newSong.put("title", As.getString("title"));
                        newSong.put("id",As.getString("id"));
                        newSong.put("name",name);
                        Songs.add(newSong);
                    }

                }

            } catch (MalformedURLException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch(JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void unusedParameter) {

            /* Tell the adapter that we have a new data to render */
            SongAdapter.notifyDataSetChanged();
        }
    }
}